const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
const morgan = require('morgan');
const port = 8080;

app.use(express.json());
app.get('/api/files', (req, res) => {
  fs.access('./api/files', fs.F_OK, (err) => {
    if (err) return res.status(500).json({ message: 'Server error' });
    else
      fs.readdir('./api/files', (err, files) => {
        if (err) return res.status(400).json({ message: 'Client error' });
        else if (files.length === 0)
          return res
            .status(200)
            .json({ message: 'Folder does not contain any files.' });
        else
          return res
            .status(200)
            .json({ message: 'Success', files: [...files] });
      });
  });
});

app.get(`/api/files/:filename`, (req, res) => {
  const namePath = req.params.filename;
  fs.stat(`./api/files/${namePath}`, (err, stats) => {
    if (err)
      return res.status(400).json({
        message: `No file with '${namePath}' filename found`,
      });
    else
      fs.readFile(`./api/files/${namePath}`, 'utf8', (err, content) => {
        if (err) return res.status(500).json({ message: 'Server error' });
        else {
          const extention = path.extname(namePath).split('.');
          return res.status(200).json({
            message: 'Success',
            filename: namePath,
            content,
            extension: extention[extention.length - 1],
            uploadedDate: stats.birthtime,
            modifiedDate: stats.mtime,
          });
        }
      });
  });
});

app.post('/api/files', (req, res) => {
  const allowedFile = new RegExp(/^[A-z1-9.]+\.(log|txt|json|yaml|xml|js)$/);
  const file = req.body.filename;
  if (!allowedFile.test(file))
    return res
      .status(400)
      .json({ message: `Your filename or type is NOT supported. ` });
  else if (typeof req.body.content === 'undefined')
    return res
      .status(400)
      .json({ message: `Please specify 'content' parameter` });
  fs.writeFile(`./api/files/${file}`, req.body.content, (err) => {
    if (err) return res.status(500).json({ message: 'Server error' });
  });
  return res.status(200).json({ message: 'File created successfully' });
});

app.get('*', function (req, res) {
  res
    .status(404)
    .json({ message: `404 Sorry. The page you're looking for was not found` });
});

function errorHandler(err, req, res, next) {
  return res.status(500).json({ message: 'Server error' });
}

app.use(morgan(':method :status URL::url "HTTP/:http-version"'));
app.use(errorHandler);

app.listen(port);
